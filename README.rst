G15MPD
======

A simple frontend for the MPD Media Player Daemon, for use with g15daemon.

=======
Warning
=======
I'm discontinuing this after someone made a fuzz about a feature he decided he want a decade later. 
And as far I am concerned, Arch Linux's AUR administrators find this behavior just fine, so I'm not wasting my efforts on this anymore.
I can still fix issues as I always did and help via mail, but keep in mind Arch Linux is impossible to be supported.

========
Features
========

- Artist/Title Info is displayed
- Track time elapsed/total time is displayed, with a completion bar.
- Random and Repeat modes are available.
- Multimedia keys are usable when run from X.

============
Requirements
============

- X11
- libg15render
- libmpd
- g15daemon

=======
Caveats
=======

- Requires MPD to be running in the background.
- In order for multimedia keys to function, the frontend must be run from X11, either in a console, or at X login.
- Requires that X11 has been configured to understand the mediakeys, either via the g15daemon xmodmap file, or a similar keyboard being selected in X configuration.
- Currently, playlist management is not available, so another mpd frontend will be required to create one.
